FROM openjdk:8
COPY target/divi-crawler-1.0-SNAPSHOT-shaded.jar /usr/src/myapp/
WORKDIR /usr/src/myapp/
VOLUME /usr/src/myapp/output
CMD ["java","-jar","divi-crawler-1.0-SNAPSHOT-shaded.jar","COVID19"]
