# Crawler divi

# How to build?
Requirements: java >= 8 and maven (sudo apt install maven) or https://maven.apache.org/
 mvn clean install

# How to execute?
Requirements: java >= 8

 # for https://divi.de/register/intensivregister?view=items
 java -jar divi-crawler-1.0-SNAPSHOT-shaded.jar

 # for https://www.divi.de/images/register/report2v.html
 java -jar divi-crawler-1.0-SNAPSHOT-shaded.jar COVID-19

You will get a JSON output on Std out


## Docker Container
### build
 docker build -t divi-crawler .
 
### execute  
 docker run -it --rm --name divi divi-crawler
