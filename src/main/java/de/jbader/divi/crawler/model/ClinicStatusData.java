/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.jbader.divi.crawler.model;

import java.net.URL;
import java.util.Date;
import lombok.Data;

/**
 *
 * @author jbader
 */
@Data
public class ClinicStatusData {
    public enum Status {
        GREEN,
        YELLOW,
        RED
    }
    
    private String name;
    private String contact;
    private URL url;
    
    private String state;
    
    private Status icuLowCare;
    private Status icuHighCare;
    private Status ecmo;
    
    private Date timestamp;
}
