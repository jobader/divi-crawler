/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.jbader.divi.crawler;

/**
 *
 * @author jbader
 */
public class Main {
    
    public static void main(String[] args) throws Exception {
        if(args.length>0) {
            new Covid19ICUScraper().execute();
        }
        else {
            new ClinicStatusScraper().execute();
        }
    }
  
}
