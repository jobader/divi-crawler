/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.jbader.divi.crawler;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonWriter;
import java.io.OutputStreamWriter;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.http.HttpVersion;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


/**
 * https://www.divi.de/images/register/report2v.html
 * 
 * @author jbader
 */
public class Covid19ICUScraper {

    public void execute() throws Exception {
        
        Response response = Request.Get("http://www.divi.de/images/register/report2v.html")
                .version(HttpVersion.HTTP_1_0)
                .addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
                .addHeader("Accept-Encoding", "gzip")
                .addHeader("Accept-Language","de,en-US;q=0.7,en;q=0.3")
                .addHeader("Host","www.divi.de")
                .execute();
        
        String result = response.returnContent().asString();
        
        Document doc = Jsoup.parse(result);
        Elements body = doc.select("body");
        Element script = body.select("script").get(0);        
        String scriptText = script.toString();
        
        int beginIndex = scriptText.indexOf("var spec =")+"var spec =".length();
        int endIndex = scriptText.indexOf("var embedOpt =");
        String json = scriptText.substring(beginIndex, endIndex).trim();        
        endIndex = json.lastIndexOf(";");        
        json = json.substring(0, endIndex).trim();
        
        JsonElement jsonElement = JsonParser.parseString(json);
        JsonObject rootObject = jsonElement.getAsJsonObject();
        JsonObject arr = rootObject.getAsJsonObject("datasets");
        Set<Map.Entry<String, JsonElement>> entries = arr.entrySet();
        
        for(Map.Entry<String, JsonElement> entry: entries) {
            JsonArray entryArr = entry.getValue().getAsJsonArray();
            
            JsonWriter writer = new JsonWriter(new OutputStreamWriter(System.out));
            writer.beginObject();
            writer.name("bundesLandStatus");
            writer.beginArray();
            
            Iterator<JsonElement> it = entryArr.iterator();
            while(it.hasNext()) {
                JsonObject stateObj = it.next().getAsJsonObject();
                
                writer.beginObject();
                
                for(Map.Entry<String, JsonElement> stateValueEntry: stateObj.entrySet())  {
                    final String key = stateValueEntry.getKey();
                    JsonElement value = stateValueEntry.getValue();
                    
                    switch(key) {
                        case "type":
                        case "bundesland":
                        case "RS":
                        case "RS_ALT":
                            writer.name(key).value(value.getAsString());
                            break;
                            
                        case "geometry":
                        case "SHAPE_AREA":
                        case "SHAPE_LENG":
                            break;
                            
                        case "covid_rel":
                        case "lat":
                        case "lon":
                        case "osm_id":
                            writer.name(key).value(value.getAsBigDecimal());
                            break;
                            
                        default:
                            writer.name(key).value(value.getAsInt());
                    }
                }
                
                writer.endObject();
            }
            
            writer.endArray();

            writer.endObject();
            writer.close();
        }
    }

   
}
